#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"
#include "kmers.h"
#include "SWAlgorithm.h"

#define ALL_ZEROS '.'

#define K 100
#define W 100

void align(struct KmerArray minimizers, struct DynStr read,
           struct DynStr ref, int k, struct Finds* finds_buffer){
    struct MatchNode* matches;
    struct MaxCell score, maxscore;
    char* ref_p; char* read_p;
    char* max_ref_p; char* max_read_p;
    unsigned long ofrf, ofrd, max_ofrf;
    unsigned long mrd, mrf;
    unsigned long lrd, lrf;
    unsigned long len, max_len;

    struct cell* m;
    struct cell* shadow_m;

    maxscore.maxcost = -1;

    matches = find_matches(minimizers, read, k);
    if(matches == NULL)
        return;
    m = malloc((read.length+1)*(read.length+1)*sizeof(struct cell));
    shadow_m = malloc((read.length+1)*(read.length+1)*sizeof(struct cell));
    while(matches != NULL){
        ref_p = ref.data;
        read_p = read.data;

        mrf = matches -> ref_index;
        mrd = matches -> read_index;

        lrd = read.length;
        lrf = ref.length;

        if(mrd > mrf){
            ofrf = 0;
            ofrd = mrd-mrf;
            len = lrd - ofrd;
        } else if(lrf-(mrf-mrd) < lrd) {
            ofrd = 0;
            ofrf = mrf-mrd;
            len = lrf - ofrf;
        } else {
            ofrf = mrf-mrd;
            ofrd = 0;
            len = lrd;
        }

        ref_p += ofrf;
        read_p += ofrd;

        score = SWalgorithm(m, ref_p, read_p, len, len);
        if(score.maxcost > maxscore.maxcost){
            maxscore = score;
            memcpy(shadow_m, m, (read.length+1)*(read.length+1)*sizeof(struct cell));
            //printMatrix(m, 1, read_p, ref_p, len, len);
            max_ref_p = ref_p; max_read_p = read_p;
            max_ofrf = ofrf;
            max_len = len;
        }
        matches = matches -> next_node;
    }
    free(m);
    reconstruct_path(shadow_m, max_ref_p, max_read_p, maxscore.i, maxscore.j, max_len+1,
                     finds_buffer+max_ofrf);
    free(shadow_m);
}

char find_max(struct Finds finds){
    char max_char = ALL_ZEROS;
    unsigned int max = 0;
    unsigned int* first_elem = &(finds.del);
    int i;

    char chars[] = {'-', 'A', 'T', 'C', 'G', 'a', 't', 'c', 'g'};

    for(i=0;i<sizeof chars;i++){
        if(first_elem[i] > max && first_elem[i] > 1){
            max_char = chars[i];
            max = first_elem[i];
        }
    }

    return max_char;
}

void output(struct Finds* find_buffer, struct DynStr ref){
    unsigned long i;
    char max_char;
    unsigned long len = ref.length;
    /* for(i=0;i<len;i++){ */
    /*     printf("del=%u, sA=%u, sT=%u, sC=%u, sG=%u, iA=%u, iT=%u, iC=%u, iG=%u\n", */
    /*             find_buffer[i].del, */
    /*             find_buffer[i].sub_A, */
    /*             find_buffer[i].sub_T, */
    /*             find_buffer[i].sub_C, */
    /*             find_buffer[i].sub_G, */
    /*             find_buffer[i].ins_A, */
    /*             find_buffer[i].ins_T, */
    /*             find_buffer[i].ins_C, */
    /*             find_buffer[i].ins_G); */
    /* } */
    for(i=0;i<len;i++){
        max_char = find_max(find_buffer[i]);
        if(max_char == ALL_ZEROS)
            continue;
        else if(max_char == '-'){
            printf("D,%lu,-\n", i);
        } else if(!(max_char & 0x20)){
            if(ref.data[i] == max_char)
                continue;
            printf("X,%lu,%c\n", i, max_char);
        } else {
            printf("I,%lu,%c\n", i, max_char ^ 0x20);
        }
    }
}

int main(int argc, char* argv[]){
    FILE *file;
    int i=0;
    struct KmerArray minimizers;
    struct DynStr ref, read;

    struct Finds* find_buffer;

    dynstr_init(&ref);
    file = fopen(argv[1], "r");
    read_next_FASTA_seq(&ref, file);
    fclose(file);

    minimizers = compute_minimizers(ref, W, K, 0);
    qsort(minimizers.elements, minimizers.length, KMER_SIZE, compare_kmers);

    find_buffer = calloc(ref.length, sizeof(struct Finds));

    file = fopen(argv[2], "r");
    dynstr_init(&read);

    do{
        dynstr_erase(&read);
        read_next_FASTA_seq(&read, file);
        align(minimizers, read, ref, K, find_buffer);
    } while(read.length != 0UL);
    output(find_buffer, ref);
    return 0;

}
