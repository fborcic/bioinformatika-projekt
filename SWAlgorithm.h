#define MATCH 0
#define INSERT 1
#define DELETE 2
#define MAXLEN 101
#define GAP_COST -2 //TODO ove brojke zbog provjere(link), treba promjeniti?!
#define MATCH_COST 2
#define MISMATCH_COST -1

struct cell {
	int cost; /* cost of reaching this cell */
	int parent; /* parent cell */
};

struct MaxCell {
    unsigned long i;
    unsigned long j;
    int maxcost;
};

struct Finds {
    unsigned int del;

    unsigned int sub_A;
    unsigned int sub_T;
    unsigned int sub_C;
    unsigned int sub_G;

    unsigned int ins_A;
    unsigned int ins_T;
    unsigned int ins_C;
    unsigned int ins_G;
};

int match(char c, char d);
void cell_init(struct cell* m, unsigned long i, unsigned long j, unsigned long rowlen);
void goal_cell(struct cell* m, char *s, char *t, unsigned long *x, unsigned long *y);
void match_out(char *s, char *t, unsigned long i, unsigned long j, struct Finds* find_buffer);
void insert_out(char *t, unsigned long j, struct Finds* find_buffer);
void delete_out(char *s, unsigned long i, struct Finds* find_buffer);
void reconstruct_path(struct cell* m, char *s, char *t, unsigned long i,
                      unsigned long j, unsigned long rowlen, struct Finds* find_buffer);
void printMatrix(struct cell* m, int flag,char *s, char *t,
                      unsigned long s_len, unsigned long t_len);
struct MaxCell SWalgorithm(struct cell* m, char *s, char *t,
                           unsigned long s_len, unsigned long t_len);
