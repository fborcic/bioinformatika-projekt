#include<stddef.h>
#include<string.h>
#include<stdlib.h>
#include<stdio.h>

#include "parser.h"
#include "kmers.h"
#include "SWAlgorithm.h"

//struct cell m[MAXLEN][MAXLEN]; /* dynamic programming table  */ // TODO kako izbjec da je ovo globalna varijabla?

int match(char c, char d) {
	if (c == d) return(MATCH_COST);
	else return(MISMATCH_COST);
}
void cell_init(struct cell* m, unsigned long i, unsigned long j, unsigned long rowlen){
	m[i*rowlen+j].cost = 0;
	m[i*rowlen+j].parent = -1;
}

/* //TODO doslovno se cijeli fija moze izbjec samo ako se pamti najveci cost i indexi kod prvog prolaza... je li to ispravno? */
/* void goal_cell(struct cell* m, char *s, char *t,unsigned long *x, unsigned long *y) { */
/* 	unsigned long i,j; */
/*     unsigned long rowlen = strlen(s)+1; */
/* 	*x = *y = 0; */
/* 	for (i=0; i<strlen(s)+1; i++){ */
/* 		for (j=0; j<strlen(t)+1; j++){ */
/* 			if (m[i*rowlen+j].cost > m[*x*rowlen+*y].cost) { */
/* 				*x = i; */
/* 				*y = j; */
/* 			} */
/* 		} */
/* 	} */
/* } */


void match_out(char *s, char *t, unsigned long i, unsigned long j, struct Finds* find_buffer){
	/* if (s[i-1] == t[j-1]) printf("M"); */
	/* else printf("S"); */
    switch(t[j-1]){
        case 'A':
            find_buffer[i-1].sub_A++;
            break;
        case 'T':
            find_buffer[i-1].sub_T++;
            break;
        case 'C':
            find_buffer[i-1].sub_C++;
            break;
        case 'G':
            find_buffer[i-1].sub_G++;
            break;
        default:
            puts("Something's not right");
    }
}

void insert_out(char *t, unsigned long j, struct Finds* find_buffer){
	/* printf("I"); */
    switch(t[j-1]){
        case 'A':
            find_buffer[j-1].ins_A++;
            break;
        case 'T':
            find_buffer[j-1].ins_T++;
            break;
        case 'C':
            find_buffer[j-1].ins_C++;
            break;
        case 'G':
            find_buffer[j-1].ins_G++;
            break;
        default:
            puts("Something's not right");
    }
}

void delete_out(char *s, unsigned long i, struct Finds* find_buffer){
    /* printf("D"); */
    find_buffer[i-1].del++;
}

//TODO promjenit sve *_out fije da pisu u buffer umjesto printa
void reconstruct_path(struct cell* m, char *s, char *t, unsigned long i,
                      unsigned long j, unsigned long rowlen, struct Finds* find_buffer){
    unsigned long r = rowlen;
	if (m[i*rowlen+j].parent == -1)
        return;
	if (m[i*rowlen+j].parent == MATCH) {
		reconstruct_path(m,s,t,i-1,j-1,r,find_buffer);
		match_out(s, t, i, j, find_buffer);
		return;
	}
    if (m[i*rowlen+j].parent == INSERT) {
        reconstruct_path(m,s,t,i,j-1,r,find_buffer);
		insert_out(t,j,find_buffer);
		return;
    }
    if (m[i*rowlen+j].parent == DELETE) {
        reconstruct_path(m,s,t,i-1,j,r,find_buffer);
		delete_out(s,i,find_buffer);
		return;
    }
}

void printMatrix(struct cell* m, int flag,char *s, char *t,
                 unsigned long s_len, unsigned long t_len){
	unsigned long i,j,x,y,rowlen;
	x = s_len;
    rowlen = x+1;
	y = t_len;
	printf("   ");
	for (i=0; i<=y; i++)
        if(!i)
            printf("  |");
        else
            printf("  %c",t[i-1]);
	printf("\n");
	for (i=0; i<=x; i++){
        if(!i)
            printf("-: ");
        else
		    printf("%c: ",s[i-1]);
		for (j=0; j<=y; j++){
			if (flag)
				printf(" %2d",m[i*rowlen+j].cost);
			else
				printf(" %2d",m[i*rowlen+j].parent);
		}
		printf("\n");
	}
}

struct MaxCell SWalgorithm(struct cell* m, char *s, char *t,
                           unsigned long s_len, unsigned long t_len){ /*opt manje strlenova*/
	unsigned long i,j,k,rowlen;
    struct MaxCell maxcell;
	int opt[3]; /* cost of the three options */
    rowlen = s_len + 1;
    maxcell.maxcost = -1;
	for (i=0; i<=s_len; i++)
        cell_init(m,i,0,rowlen);
    for (j=0; j<=t_len; j++)
        cell_init(m,0,j,rowlen);

	//printf("%s , %s\n",s,t);
	for (i=0; i<s_len; i++){
		for (j=0; j<t_len; j++) {
            cell_init(m,i+1,j+1,rowlen);
			opt[MATCH] = m[i*rowlen+j].cost + match(s[i],t[j]);
			opt[INSERT] = m[(i+1)*rowlen+j].cost + GAP_COST;
			opt[DELETE] = m[i*rowlen+j+1].cost + GAP_COST;
			m[(i+1)*rowlen+j+1].cost = 0;
			for (k=MATCH; k<=DELETE; k++){
				if (opt[k] > m[(i+1)*rowlen+j+1].cost) {
					m[(i+1)*rowlen+j+1].cost = opt[k];
					m[(i+1)*rowlen+j+1].parent = k;
				}
			}
            if(m[(i+1)*rowlen+j+1].cost > maxcell.maxcost){
                maxcell.maxcost = m[(i+1)*rowlen+j+1].cost;
                maxcell.i = i+1; maxcell.j = j+1;
            }
		}
	}
	return maxcell;
}

//https://www.researchgate.net/figure/An-example-of-local-alignment-Two-sequences-s-a-and-s-b-are-aligned-in-this-example_fig12_282608028
//na linku postoji tablica s kojojm sam usporedjivo rezultate....
//TODO radi samo ako nis pocinje s prazninom?!
/* int main(void){ */
/* 	unsigned long i,j; */
/*     struct cell* m; */
/*     struct MaxCell maxcell; */
/* 	char s[] = "CACGTGATCAA"; */
/* 	char t[] = "AGCATCGGTTG"; */
/*     m=malloc((strlen(s)+1)*(strlen(t)+1)); */
/* 	maxcell = SWalgorithm(m,s,t,strlen(s),strlen(t)); */
/* 	printf("%d %lu %lu\n", maxcell.maxcost, maxcell.i, maxcell.j); */
/* 	printMatrix(m,1,s,t,strlen(s),strlen(t)); */
/* 	reconstruct_path(m,s, t, maxcell.i, maxcell.j, strlen(s)+1); */
/* } */
