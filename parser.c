#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include "parser.h"


int dynstr_init(struct DynStr* str){
    str -> data = malloc(MINLEN);
    if(str -> data == NULL)
        return -1;
    *(str -> data) = 0;
    str -> length = 0;
    str -> space = MINLEN;
    return 0;
}

int _dynstr_extend(struct DynStr* str){
    unsigned long new_size;
    new_size = (str -> space) * EXTEND_FACTOR;
    str -> data = realloc(str -> data, new_size);
    if(str -> data == NULL)
        return -1;
    str -> space = new_size;
    return 0;
}

void dynstr_erase(struct DynStr* str){
    *(str -> data) = 0;
    str -> length = 0;
}

int dynstr_append(struct DynStr* str, char* suffix){
    if(suffix == NULL){
        return -2;
    }
    unsigned long suffix_length = strlen(suffix);
    while (suffix_length + str -> length >= str -> space){
        if(_dynstr_extend(str) != 0)
            return -1;
    }
    strcat(str->data, suffix);
    str -> length += suffix_length;
    return 0;
}

void dynstr_free(struct DynStr* str){
    free(str -> data);
}

int check_next_ignore(FILE* file){
    char next[2];
    char* result;
    if(feof(file))
        return 1;
    unsigned long saved_pos = ftell(file);
    result = fgets(next, 2, file);
    if(result == NULL)
        return 1;
    fseek(file, saved_pos, SEEK_SET);
    return next[0] == FASTA_IGNORE;
}

/* optimizacija: umjesto strchr pretrazi od nazad */
char read_line(struct DynStr* sequence, FILE* file){
    char read_buffer[READ_BUFFER_LEN];
    char *t;
    do {
        fgets(read_buffer, READ_BUFFER_LEN, file);
        t = strchr(read_buffer, '\n');
        if(t != NULL) *t = 0;
        if(dynstr_append(sequence, read_buffer) != 0)
            return -1;
    } while(t == NULL && !feof(file));
    return 0;
}

/* valjda radi */
int read_next_FASTA_seq(struct DynStr* sequence, FILE* file){
    if(feof(file))
        return -2;
    while(check_next_ignore(file))
        read_line(sequence, file);
    dynstr_erase(sequence);
    do {
        if(read_line(sequence, file) != 0)
            return -1;
    } while(!check_next_ignore(file));
    return 0;
}

/* int main(void){ */
/*     FILE * file = fopen("testtest.txt", "r"); */
/*     struct DynStr str; */

/*     dynstr_init(&str); */
/*     printf("%lu\n", str.length); */

/*     read_next_FASTA_seq(&str, file); */
/*     printf("%lu\n", str.length); */
/*     return 0; */

/* } */
/* int main(void){ */
/*     struct DynStr str; */
/*     unsigned long oldsize; */
/*     FILE *file = fopen("test4.txt", "r"); */

/*     dynstr_init(&str); */
/*     dynstr_erase(&str); */
/*     read_next_FASTA_seq(&str, file); */
/*     puts(str.data); */
/*     /1* printf("%lu\n", str.length); *1/ */
/*     /1* printf("%zu\n", strlen(str.data)); *1/ */
/*     /1* dynstr_erase(&str); *1/ */
/*     /1* read_next_FASTA_seq(&str, file); *1/ */
/*     /1* printf("%lu\n", str.length); *1/ */
/*     /1* printf("%zu\n", strlen(str.data)); *1/ */
/*     /1* dynstr_free(&str); *1/ */
/*     return 0; */
/* } */

