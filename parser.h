#define MINLEN 2048
#define EXTEND_FACTOR 1.8
#define READ_BUFFER_LEN 100
#define FASTA_IGNORE '>'

struct DynStr {
    char *data;
    unsigned long length;
    unsigned long space;
};

int dynstr_init(struct DynStr*);
int dynstr_append(struct DynStr*, char*);
void dynstr_free(struct DynStr*);
int read_first_sequence(struct DynStr* sequence, FILE* file);
int check_next_ignore(FILE* file);
char read_line(struct DynStr* sequence, FILE* file);
int read_next_FASTA_seq(struct DynStr* sequence, FILE* file);
void dynstr_erase(struct DynStr* str);
