#define MINLEN 2048
#define EXTEND_FACTOR 1.8
#define KMER_SIZE sizeof(struct Kmer)

struct Kmer {
    char* kmer;
    short belongs_to;
    unsigned long position;
};

struct KmerArray {
    struct Kmer* elements;
    unsigned long length;
    unsigned long space;
};

struct MatchNode {
    unsigned long ref_index;
    unsigned long read_index;
    struct MatchNode* next_node;
};

int kmerarray_init(struct KmerArray* array);
int _kmerarray_extend(struct KmerArray* array);
int kmerarray_add(struct KmerArray* array, struct Kmer kmer);
void kmerarray_free(struct KmerArray* array);
int compare_kmers(const void* kmer1, const void* kmer2);
struct KmerArray get_kmers(const char* sequence, int k, int w,
                           unsigned long start_position, short belongs_to);
void add_minimizers(struct KmerArray* minimizers, struct KmerArray* window);
struct KmerArray compute_minimizers(struct DynStr sequence, int w, int k, short belongs_to);
struct MatchNode* find_matches(struct KmerArray minimizers, struct DynStr read, int k);
