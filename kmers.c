#include<stddef.h>
#include<string.h>
#include<stdlib.h>
#include<stdio.h>

#include "parser.h"
#include "kmers.h"


int kmerarray_init(struct KmerArray* array){
    array -> elements = malloc(MINLEN*KMER_SIZE);
    if(array -> elements == NULL)
        return -1;
    array -> space = MINLEN;
    array -> length = 0;
    return 0;
}

int _kmerarray_extend(struct KmerArray* array){
    unsigned long new_size;
    new_size = (array -> space) * EXTEND_FACTOR;
    array -> elements = realloc(array -> elements, new_size*KMER_SIZE);
    if(array -> elements == NULL)
        return -1;
    array -> space = new_size;
    return 0;
}


int kmerarray_add(struct KmerArray* array, struct Kmer kmer){
    struct Kmer kmer_copy;
    if (array -> length + 1 > array -> space){
        if(_kmerarray_extend(array) != 0)
            return -1;
    }
    kmer_copy = kmer;
    kmer_copy.kmer = malloc(strlen(kmer.kmer)+1); /*opt?*/
    strcpy(kmer_copy.kmer, kmer.kmer);
    array -> elements[array -> length] = kmer_copy;
    array -> length++;
    return 0;
}

void kmerarray_free(struct KmerArray* array){
    unsigned long i;
    for(i=0;i<array->length;i++)
        free(array -> elements[i].kmer);
    free(array->elements);
}

int compare_kmers(const void* kmer1, const void* kmer2){
    return strcmp(((const struct Kmer*) kmer1)-> kmer,
                  ((const struct Kmer*) kmer2)-> kmer);
}

struct KmerArray get_kmers(const char* sequence, int k, int w,
                           unsigned long start_position, short belongs_to){
    struct KmerArray ary;
    struct Kmer kmer;
    int i;
    size_t length = strlen(sequence);
    kmerarray_init(&ary);
    for(i=0;i<w;i++){
        if(i+k>length)
            break;
        kmer.kmer = malloc(k+1);
        strncpy(kmer.kmer, sequence+i, k);
        kmer.kmer[k]=0;
        kmer.position = start_position+i;
        kmer.belongs_to = belongs_to;
        kmerarray_add(&ary, kmer);
    }
    return ary;
}

/*funkcija mijenja window*/
void add_minimizers(struct KmerArray* minimizers, struct KmerArray* window){
    char* minimizer;
    int i;
    qsort(window->elements, window->length, KMER_SIZE, compare_kmers);
    minimizer = window->elements[0].kmer;
    i=0;

    while(strcmp(minimizer, window->elements[i].kmer) == 0){
        kmerarray_add(minimizers, window->elements[i]);
        i++;
    }
}

struct KmerArray compute_minimizers(struct DynStr sequence, int w, int k, short belongs_to){
    char* seq_data;
    struct KmerArray tempor_ary, minimizers;
    unsigned long processed = 0;

    kmerarray_init(&minimizers);
    seq_data = sequence.data;
    while(processed+w+k-1 < sequence.length){
        tempor_ary = get_kmers(seq_data, k, w, processed, belongs_to);
        add_minimizers(&minimizers, &tempor_ary);
        kmerarray_free(&tempor_ary);

        processed += w;
        seq_data += w;
    }

    return minimizers;
}

long find_kmer(struct KmerArray sorted_kmers, struct Kmer kmer){
    long len = sorted_kmers.length;
    long lowerBound = 0;
    long upperBound = len -1;
    long midPoint = -1;
    long index = -1;
    int res;

    while(lowerBound <= upperBound) {
      midPoint = lowerBound + (upperBound - lowerBound) / 2;
      res = strcmp(sorted_kmers.elements[midPoint].kmer,kmer.kmer);
      if(!res) {
         index = midPoint;
         break;
      } else {
         // if data is larger
         if(res < 0) {
            // data is in upper half
            lowerBound = midPoint + 1;
         }
         // data is smaller
         else {
            // data is in lower half
            upperBound = midPoint -1;
         }
      }
    }
    while(index > 0 && index < len &&
            !strcmp(sorted_kmers.elements[index - 1].kmer,kmer.kmer)){
        index--;
    }
   return index;
}

struct MatchNode* find_matches(struct KmerArray minimizers, struct DynStr read, int k){
    struct KmerArray read_kmers;
    struct MatchNode* start = NULL;
    struct MatchNode* current;
    struct MatchNode** last = &start;

    int i;
    unsigned long index;

    //printf("%d", strlen(read.data));

    read_kmers = get_kmers(read.data, k, read.length-k+1, 0, 0x00B5);
    for(i=0;i<read_kmers.length;i++){
        index = find_kmer(minimizers,read_kmers.elements[i]);
        if(index == -1) continue;
        do{
            current = malloc(sizeof(struct MatchNode));
            current -> ref_index = minimizers.elements[index].position;
            current -> read_index = read_kmers.elements[i].position;
            current -> next_node = NULL;
            *last = current;
            last = &(current -> next_node);
        } while(index < minimizers.length &&
                strcmp(minimizers.elements[index++].kmer, read_kmers.elements[i].kmer));
    }
    kmerarray_free(&read_kmers);
    return start;
}


/*int main(void){
    int i;
    struct KmerArray window, ary;
    struct Kmer km;
    for(i=0;i<100;i++){
        km.kmer = malloc(10);
        strcpy(km.kmer, KMERS[rand()%5]);
        km.position = i;
        km.belongs_to = i+2;
        kmerarray_add(&ary, km);
    }
    kmerarray_init(&ary);
    window = get_kmers("1231239999999999999", 3, 5, 20, 0);
    add_minimizers(&ary, &window);
    for(i=0;i<ary.length;i++){
        printf("%s, %lu\n", ary.elements[i].kmer, ary.elements[i].position);
    }
    kmerarray_free(&ary);
    return 0;
}*/

/* int main(void){ */
/*     FILE *file; */
/*     int i; */
/*     struct KmerArray minimizers; */
/*     struct DynStr str; */
/*     struct MatchNode *matches = NULL; */

/*     dynstr_init(&str); */
/*     file = fopen("test3.txt", "r"); */
/*     read_next_FASTA_seq(&str, file); */
/*     minimizers = compute_minimizers(str, 20, 20, 0); */
/*     for(i=0;i<minimizers.length;i++){ */
/*         printf("%s, %lu\n", minimizers.elements[i].kmer, minimizers.elements[i].position); */
/*     } */
/*     qsort(minimizers.elements, minimizers.length, KMER_SIZE, compare_kmers); */
/*     printf("%lu\n", minimizers.length); */
/*     fclose(file); */
/*     file = fopen("test4.txt", "r"); */
/*     dynstr_erase(&str); */
/*     read_next_FASTA_seq(&str, file); */
/*     matches = find_matches(minimizers, str, 20); */
/*     while(matches != NULL){ */
/*         printf("%lu, %lu\n", matches->ref_index, matches->read_index); */
/*         matches = matches -> next_node; */
/*     } */
/*     return 0; */

/* } */
